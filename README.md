# Call Number Training/Game

## Description
Based on a discussion with a coworker at the University of Dayton Roesch Library regarding Library of Congress Call Number training/games, I thought I would put together a simple version that should be deployable with Isidore or LibGuides by referencing a single JavaScript file and some CDN resources (if hosting is limited).

This project was inspired by and took some design cues from the following projects:
* [Kent State University - Library of Congress Tutorial - Call Number and Shelving](https://www.library.kent.edu/university-libraries/library-congress-tutorial-call-number-and-shelving)
* [Carnegie Mellon University Libraries - Flash Games - Within Range](https://digitalpreservation.library.cmu.edu/flash/etc/)

## Visuals
![Gif of the Call Number Training interface fixing the order of a bookshelf](informational/call_number_training_08092023.gif "Call Number Training")

## Making Changes

### Images
* Install the VCR font (`dist/assets/fonts/VCR_OSD_MONO_1.001.ttf`).
* Open the `svg` directory for the source image files as SVG.
  * `bookshelf-labels_repixel.svg` should be used to update the third portion of the quiz where you move books to their appropriate part of the library.

### Books
* To change the books in each part of the quiz, update the variables in `dist/assets/js/main.js`. The `shelvingOrder` is the correct answer. The order of the objects will be used when creating the quiz (so you have to randomize yourself).
* For the third part, the books must go to the correct shelf. Instead of a numerical `shelvingOrder` these are given the shelf name. The `shelvingOrder` must be one of the shelves (specified after the first shelf).

### Example
We have changed our collections in the library, the LTC books have been relocated or removed. The game needs to be updated to reflect this:

* Update `bookshelf-labels_repixel.svg` so the labels are correct. Export as a PNG and replace `bookshelf-signs-labels.png`.
* Update `dist/assets/js/main.js` so variable `bookshelfThree` has the correct shelves and `shelvingOrder`.
* Test the `dist` version to confirm things are working as expected.
* Copy and replace the `dist/assets` directory into the `scorm` directory.
* Create a ZIP file containing the `scorm` directory, this will is used in Canvas.

## Authors and acknowledgment

### Tools/Libraries
* [Dragula](https://github.com/bevacqua/dragula) - to handle the book dragging and reordering
* [jsPDF](https://github.com/parallax/jsPDF) - to generate a PDF certificate at the end
* [Bootstrap](https://getbootstrap.com/) - generic styling, LibGuides uses Bootstrap
* [Yoksel URL-encoder for SVG](https://yoksel.github.io/url-encoder/) - used to convert SVG elements to CSS `background-image`

### Images
* Bookshelf Design: I modified a design by [MahuaSarkar](https://pixabay.com/vectors/bookshelf-empty-design-shelf-2907509/) to make the bookshelf more flat and pixelated and added in the paper sign for each shelf.
* Font: ["VCR OSD Mono" from dafont](https://www.dafont.com/vcr-osd-mono.font)
* Certificate: Uses the "VCR OSD Mono" font, but the certificate was created in Inkscape based on pixel patterns ([inspired by the top left](https://www.123rf.com/photo_40390354_vector-geometric-elements-pixel-art-collection-minimalism-symbol-abstract-pattern-for-design-logo.html)). Free for all to use.
* Book Design Options:
  * I previously used Freepik's spines, but have been migrating to a retro/pixelated look and decided to create very basic spines in Inkscape. These files are free for all to use.
  * [Freepik](https://www.freepik.com/free-vector/hand-drawn-flat-design-book-spine-illustration_23969503.htm): Flat book spine designs with an Art Deco vibe.

## License
MIT License - Use it as you see fit!
