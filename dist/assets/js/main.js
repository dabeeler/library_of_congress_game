/* global bootstrap, jspdf, CallNumberTrainer, setComplete */
/* exported firstTest, initCallNumber */

const bookshelfOne = [
    {
        'shelf': [
            {
                'callNumber': 'LC\n6515\n.B55\n2000',
                'cover': 'grey',
                'shelvingOrder': 1,
            },
            {
                'callNumber': 'LD\n2160\n.L54\n2001',
                'cover': 'grey',
                'shelvingOrder': 2,
            },
        ],
        'helptext': [
            'The Letter Line should be filed alphabetically (LC before LC)',
        ],
    },
    {
        'shelf': [
            {
                'callNumber': 'R\n146.9\n.B9',
                'cover': 'indigo',
                'shelvingOrder': 2,
            },
            {
                'callNumber': 'R\n146.6\n.N49',
                'cover': 'indigo',
                'shelvingOrder': 1,
            },
        ],
        'helptext': [
            'Sometimes the second line is decimalized (146.6 before 146.9)',
        ],
    },
    {
        'shelf': [
            {
                'callNumber': 'B\n945\n.J2\nP4\nv.2',
                'cover': 'indigo',
                'shelvingOrder': 2,
            },
            {
                'callNumber': 'B\n945\n.J2\nP4',
                'cover': 'indigo',
                'shelvingOrder': 1,
            },
        ],
        'helptext': [
            'Nothing comes before something (nothing before v.2)',
        ],
    },
    {
        'shelf': [
            {
                'callNumber': 'Q\n284\n.3\nK94x',
                'cover': 'red',
                'shelvingOrder': 2,
            },
            {
                'callNumber': 'Q\n284.3\n.K94',
                'cover': 'red',
                'shelvingOrder': 1,
            },
        ],
        'helptext': [
            '\'x\' is treated as a half (K94 before K94x)',
        ],
    },
];

const bookshelfTwo = [
    {
        'shelf': [
            {
                'callNumber': 'LC\n1085\n.2\n.B63\n1994',
                'cover': 'purple',
                'shelvingOrder': 3,
            },
            {
                'callNumber': 'LC\n1085\n.2\n.B424\n1993',
                'cover': 'grey',
                'shelvingOrder': 1,
            },
            {
                'callNumber': 'LC\n1085\n.2\n.B461\n1993',
                'cover': 'indigo',
                'shelvingOrder': 2,
            },
            {
                'callNumber': 'LC\n1085\n.2\n.L8x\n1992',
                'cover': 'red',
                'shelvingOrder': 4,
            },
            {
                'callNumber': 'LC\n1091\n.M68\n1994',
                'cover': 'teal',
                'shelvingOrder': 5,
            },
        ],
    },
    {
        'shelf': [
            {
                'callNumber': 'HQ\n72\n.A2',
                'cover': 'indigo',
                'shelvingOrder': 2,
            },
            {
                'callNumber': 'HQ\n72\n.A2\n1988\nv.2',
                'cover': 'purple',
                'shelvingOrder': 4,
            },
            {
                'callNumber': 'HQ\n72\n.A18',
                'cover': 'grey',
                'shelvingOrder': 1,
            },
            {
                'callNumber': 'HQ\n72\n.A2\nv.2',
                'cover': 'red',
                'shelvingOrder': 3,
            },
            {
                'callNumber': 'HQ\n73\n.A19\nG3',
                'cover': 'teal',
                'shelvingOrder': 5,
            },
        ],
        'helptext': [
            'Nothing before something',
            'Compare from the top, starting with letters and then whole numbers',
        ],
    },
    {
        'shelf': [
            {
                'callNumber': 'Q\n284.3\n.K94',
                'cover': 'red',
                'shelvingOrder': 4,
            },
            {
                'callNumber': 'Q\n284\n.3\nK94x',
                'cover': 'grey',
                'shelvingOrder': 5,
            },
            {
                'callNumber': 'Q\n284\n.2\n.R17',
                'cover': 'purple',
                'shelvingOrder': 2,
            },
            {
                'callNumber': 'Q\n284.2\n.R2',
                'cover': 'indigo',
                'shelvingOrder': 3,
            },
            {
                'callNumber': 'Q\n284.2\n.L94',
                'cover': 'teal',
                'shelvingOrder': 1,
            },
        ],
        'helptext': [
            'When the decimal of the topic number is on the third line, it is still part of the topic number',
            '\'x\' is treated as a half',
        ],
    },
    {
        'shelf': [
            {
                'callNumber': 'HB\n103\n.B5',
                'cover': 'teal',
                'shelvingOrder': 2,
            },
            {
                'callNumber': 'HD\n97\n.A33',
                'cover': 'red',
                'shelvingOrder': 4,
            },
            {
                'callNumber': 'HB\n102\n.B6',
                'cover': 'grey',
                'shelvingOrder': 1,
            },
            {
                'callNumber': 'HD\n101\n.B52',
                'cover': 'indigo',
                'shelvingOrder': 5,
            },
            {
                'callNumber': 'HC\n100\n.B53\nA9',
                'cover': 'purple',
                'shelvingOrder': 3,
            },
        ],
    },
    {
        'shelf': [
            {
                'callNumber': 'J\n133\n.S42\n1954',
                'cover': 'grey',
                'shelvingOrder': 4,
            },
            {
                'callNumber': 'J\n133\n.S33\n1950',
                'cover': 'teal',
                'shelvingOrder': 3,
            },
            {
                'callNumber': 'J\n133\n.J32\n1948',
                'cover': 'indigo',
                'shelvingOrder': 1,
            },
            {
                'callNumber': 'J\n133\n.S5\n1954',
                'cover': 'purple',
                'shelvingOrder': 5,
            },
            {
                'callNumber': 'J\n133\n.S3\nvol.2',
                'cover': 'red',
                'shelvingOrder': 2,
            },
        ],
    },
];

const bookshelfThree= [
    {
        'shelf': [
            {
                'callNumber': 'CT\n275\n.S8987\nA3\n2023', // GREEN LABEL BOOK
                'cover': 'grey green-label',
                'shelvingOrder': 'LEISURE',
            },
            {
                'callNumber': 'TEMP\nGR.STOR\nUS.CATH\nBR\n515\n.D53\n1990',
                'cover': 'indigo',
                'shelvingOrder': 'GR.STOR',
            },
            {
                'callNumber': 'LOBBY\nHQ\n75.13\nD38\n2021',
                'cover': 'purple',
                'shelvingOrder': 'LEISURE',
            },
            {
                'callNumber': 'REF\nG\n1021\n.A7545\n2010\nOVERSIZE',
                'cover': 'red',
                'shelvingOrder': 'REFERENCE',
            },
            {
                'callNumber': 'Mar.Lib.\nBR\n251\n.C6\nC6\nv.198',
                'cover': 'indigo',
                'shelvingOrder': 'Mar.Lib.',
            },
            {
                'callNumber': 'TEMP\nGR.STOR\nUS.CATH\nKBU\n2872\n.C54\n1944',
                'cover': 'teal',
                'shelvingOrder': 'GR.STOR',
            },
            {
                'callNumber': 'REF\nHQ\n1143\n.W643\n2004\nv.2',
                'cover': 'grey',
                'shelvingOrder': 'REFERENCE',
            },
            {
                'callNumber': 'Mar.Lib.\nBR600\n.O94\n2020',
                'cover': 'purple',
                'shelvingOrder': 'Mar.Lib.',
            },
            {
                'callNumber': 'HD\n101\n.B52',
                'cover': 'red',
                'shelvingOrder': 'RL',
            },
        ],
        'name': 'RL',
        'helptext': [
            'This shelf should only contain Roesch Library books.',
        ],
    },
    {
        'shelf': [
        ],
        'name': 'Mar.Lib.',
        'helptext': [
            'This shelf should only contain Marian Library books.',
        ],
    },
    {
        'shelf': [
        ],
        'name': 'REFERENCE',
        'helptext': [
            'This shelf should only contain Reference books.',
        ],
    },
    {
        'shelf': [
        ],
        'name': 'LEISURE',
        'helptext': [
            'This shelf should only contain Leisure Reading books.',
        ],
    },
    {
        'shelf': [
        ],
        'name': 'GR.STOR',
        'helptext': [
            'This shelf should only contain Ground Storage books.',
        ],
    },
];

/**
 * Initialize the Call Number routine.
 *
 * You must include `onLMS` as `true` if
 * you are on a Learning Management System (LMS).
 * This will have the method communicate completion
 * to the LMS and remove the PDF Certificate
 * of Completion.
 *
 * @param {boolean} onLMS Set to true if on an LMS
 */
function initCallNumber(onLMS=true) {
    // Progression Modal
    const progressModal = new bootstrap.Modal(document.querySelector('#test-progression-modal'));

    // Completion Modal (LMS ONLY)
    const completionModal = new bootstrap.Modal(document.querySelector('#completion-modal'));

    // Certificate Modal
    const certificateModal = configureCertificateModal('#certificate-modal');

    // Create first test
    document.firstTest = new CallNumberTrainer(bookshelfOne, '#test-one .bookshelf-container', false);
    document.firstTest.buildBookShelf();

    // Show Test One Instructions
    showInstruction('instructions-test-one');

    // Add the checkbutton and tie to the method
    const firstTestBtn = document.querySelector('#test-one .bookshelf-check-btn .btn-check');
    firstTestBtn.addEventListener('click', function(event) {
        const allCorrect = document.firstTest.checkBookshelf(event.target);

        if (allCorrect) {
            progressModal.show();
            document.querySelector('#test-one').style.display = 'none';
            document.querySelector('#test-two').style.display = 'block';
            // Show Test Two Instructions
            showInstruction('instructions-test-two');
            document.querySelector('.instructions-test-two').scrollIntoView();
        }
    });

    // Create second test
    document.secondTest = new CallNumberTrainer(bookshelfTwo, '#test-two .bookshelf-container', false);
    document.secondTest.buildBookShelf();

    // Add the checkbutton and tie to the method
    const secondTestBtn = document.querySelector('#test-two .bookshelf-check-btn .btn-check');
    secondTestBtn.addEventListener('click', function(event) {
        const allCorrect = document.secondTest.checkBookshelf(event.target);

        if (allCorrect) {
            // Show the modal
            progressModal.show();
            document.querySelector('#test-two').style.display = 'none';
            document.querySelector('#test-three').style.display = 'block';
            // Show Test Three Instructions
            showInstruction('instructions-test-three');
            document.querySelector('.instructions-test-three').scrollIntoView();
        }
    });

    // Create third test
    document.thirdTest = new CallNumberTrainer(bookshelfThree, '#test-three .bookshelf-container', true);
    document.thirdTest.buildBookShelf();

    // Add the checkbutton and tie to the method
    const thirdTestBtn = document.querySelector('#test-three .bookshelf-check-btn .btn-check');
    thirdTestBtn.addEventListener('click', function(event) {
        const allCorrect = document.thirdTest.checkBookshelf(event.target);

        if (allCorrect) {
            if (onLMS) {
                // Set Assessment as Complete and Passing (scorm_handler.js)
                setComplete();
                // Show the modal
                completionModal.show();
            } else {
                // Show the modal
                certificateModal.show();
            }
        }
    });
}

/**
 * Initialize the Bootstrap Modal and connections
 * to the Date and Name inputs. On the modal button
 * (in the .modal-footer) click, generate the PDF
 * for download.
 *
 * @param {String} modalID ID with #
 * @return {bootstrap.Modal}
 */
function configureCertificateModal(modalID) {
    const modal = new bootstrap.Modal(document.querySelector(modalID));
    const modalBtn = document.querySelector(`${modalID} .modal-footer button`);
    const modalInput = document.querySelector(`${modalID} .modal-body input`);
    // Set the current date
    const certDate = document.querySelector(`${modalID} .modal-body span`);
    certDate.textContent = new Date().toLocaleDateString();

    // Generate the PDF on modal button click
    modalBtn.addEventListener('click', function(event) {
        // Initialize document
        const doc = new jspdf.jsPDF({ // eslint-disable-line new-cap
            orientation: 'landscape',
            unit: 'in',
            format: 'letter',
        });
        // Add the certificate image
        const img = new Image();
        img.src = './assets/images/certificate.png';
        doc.addImage(img, 'png', 0, 0, 11, 8.5);
        // Set our font
        doc.setFont('VCR_OSD_MONO_1.001', 'normal');
        // Set the Name
        doc.setFontSize(30);
        doc.setTextColor('#171b48');
        doc.text(modalInput.value, 11/2, 4.8, {
            align: 'center',
        });
        // Set the Date
        doc.setFontSize(14);
        doc.setTextColor('#000000');
        doc.text(certDate.textContent, 9.25, 7.55, {
            align: 'center',
        });
        // Save (will download for user)
        doc.save('LibraryOfCongressTrainingCertificate.pdf');
    });

    return modal;
}

/**
 * Manage the visibility of the instruction text.
 *
 * @param {String} className without '.'
 */
function showInstruction(className) {
    // Clean
    className = className.replace('.', '');

    // Get all instructions
    const instructions = document.querySelectorAll('[class^="instructions-test-"]');

    // Add .hide to all
    instructions.forEach(function(instruction) {
        instruction.classList.add('hide');
    });

    // Show the provided instruction
    document.querySelector(`.${className}`).classList.remove('hide');
}
