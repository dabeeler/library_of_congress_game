/* global dragula */
/* exported CallNumberTrainer */

/**
 * Call Number Training
 *
 * You can use this file to display a simple call number
 * training interface, based on the Library of Congress
 * format.
 *
 * If you want to customize the problems, modify the `shelf`
 * JSON object:
 * - `callNumber`: This will be displayed on the label of the
 *   spine. Use `\n` to create the correct format.
 * - `cover`: Provide the spine type, the options are ['purple',
 *   'grey', 'indigo', 'red', 'teal']
 * - `shelvingOrder`: (Integer) The correct order for that shelf,
 *   book-by-book. The JavaScript does not determine the correct
 *   order based on the call number - just the `shelvingOrder`.
 *
 * Note: The `shelf` variable has a max limit of 5 books per shelf,
 * otherwise they will visually be beyond the "sign" in the bookshelf.
 */
class CallNumberTrainer {
    /**
     * CallNumberTrainer must be given
     * a JSON object.
     *
     * @param {Object} bookshelf
     * @param {String} containerSelector
     * @param {Boolean} canDragBetweenShelves
     */
    constructor(bookshelf, containerSelector, canDragBetweenShelves) {
        this.bookshelf = bookshelf;
        this.containerSelector = containerSelector;
        this.canDragBetweenShelves = canDragBetweenShelves ? canDragBetweenShelves : false; // eslint-disable-line max-len
        // Generate a shelf prefix for multiple tests
        this.shelfPrefix = 's' + generateUID();
    }

    /**
     * Build the bookshelf HTML and
     * append to the containerSelector.
     */
    buildBookShelf() {
        // Add the fixed check shelves button
        let html = '';

        // Build each shelf
        this.bookshelf.forEach(function(obj, index) {
            // Concat the helptext so it can be set to a data attr
            const helptext = obj.helptext ? obj.helptext.join(';') : '';
            const collection = obj.name ? obj.name : '';
            const isCollection = obj.name ? 'collection' : '';

            // Build the shelf HTML
            html +=
                `<div id="${this.shelfPrefix}-shelf${index}" class="shelf"
                      data-helptext="${helptext}"
                      data-shelf="${index}"
                      data-collection="${collection}">
                    <div class="side-panel">
                        <div class="correct-incorrect-wrapper">
                            <div class="shelf-correct hide">
                                <i></i>Correct:<span></span>
                            </div>
                            <div class="shelf-incorrect hide">
                                <i></i>Incorrect:<span></span>
                            </div>
                        </div>
                        <div class="check-msg hide"></div>
                    </div>
                    <div class="book-wrapper">
                        <div id="${this.shelfPrefix}-rearrange${index}"
                             class="drag-container">`;

            // Build each book HTML
            obj.shelf.forEach(function(book) {
                html +=
                    `<div class="book ${book.cover} ${isCollection}"
                          data-order="${book.shelvingOrder}"
                     ><span>${book.callNumber}</span></div>`;
            });

            // Close the rearrange, book-wrapper, and shelf divs
            html +=
                `       </div>
                    </div>
                </div>`;
        }.bind(this));

        // Put the shelves in the container
        const container = document.querySelector(this.containerSelector);
        container.innerHTML = html;

        if (this.canDragBetweenShelves) {
            // Any container starting with the ID is draggable to/from
            const drake = dragula([].slice.call(document.querySelectorAll(
                `[id^="${this.shelfPrefix}-rearrange"]`,
            )), {
                direction: 'horizontal',
            });

            const pageContainer = document.querySelector('html');

            drake.on('drag', function(el, source) {
                // Manage vertical scroll on drag
                const pageHeight = document.querySelector('html').clientHeight;
                document.onmousemove = function(e) {
                    const mousePosition = e.pageY - $(window).scrollTop();

                    // Scroll speed
                    const speed = 25;
                    const boundaryOffset = 20;

                    // Handle went to scroll up, down, and nullify
                    if (mousePosition < boundaryOffset) {
                        if (!window.dragInterval &&
                            window.dragDirection != -1) {
                            window.dragDirection = -1;
                            window.dragInterval = setInterval(function() {
                                pageContainer.scrollBy(0, -speed);
                            }, 20);
                        }
                    } else if (mousePosition > (pageHeight - boundaryOffset)) {
                        if (!window.dragInterval &&
                            window.dragDirection != 1) {
                            window.dragDirection = 1;
                            window.dragInterval = setInterval(function() {
                                pageContainer.scrollBy(0, speed);
                            }, 20);
                        }
                    } else if (mousePosition >= boundaryOffset &&
                               mousePosition <= (pageHeight - boundaryOffset)) {
                        clearInterval(window.dragInterval);
                        window.dragInterval = null;
                        window.dragDirection = null;
                    }
                };
            });

            // Remove the onmousemove listener and autoscroll
            drake.on('dragend', function(el) {
                document.onmousemove = null;
                clearInterval(window.dragInterval);
            });
        } else {
            // Allow dragging only on the same shelf
            this.bookshelf.forEach(function(obj, index) {
                dragula([].slice.call(document.querySelectorAll(
                    `[id="${this.shelfPrefix}-rearrange${index}"]`,
                )), {
                    direction: 'horizontal',
                });
            }.bind(this));
        }
    }

    /**
     * Check orders of all shelves in bookshelf.
     *
     * Check the order of the books on each
     * shelf, based on the button's `data-shelf`
     * attribute.
     *
     * @param {HTMLElement} el
     * @return {boolean} allCorrect
     */
    checkBookshelf(el) {
        // Initialize return
        this.allCorrect = true;

        // Get the shelves for our test
        const shelves = document.querySelectorAll(
            `${this.containerSelector} div.shelf`,
        );

        // Loop through shelves and check order
        shelves.forEach(function(el) {
            const shelf = el.getAttribute('data-shelf');
            const collection = el.getAttribute('data-collection');

            // Remove the .hide class
            const hiddenEl = document.querySelectorAll(
                `#${this.shelfPrefix}-shelf${shelf} .hide`,
            );
            for (let i = 0; i < hiddenEl.length; i++) {
                hiddenEl[i].classList.remove('hide');
            }

            // Get the books
            const books = document.querySelectorAll(
                `#${this.shelfPrefix}-rearrange${shelf} .book`,
            );

            // Get the helptext container
            const msgEl = document.querySelector(
                `#${this.shelfPrefix}-shelf${shelf} .check-msg`,
            );

            let isIncorrect = false;

            if (collection) {
                isIncorrect = this.checkCollection(books, collection); // eslint-disable-line max-len
            } else {
                isIncorrect = this.checkNumericalOrder(books); // eslint-disable-line max-len
            }

            // Handle messaging for the user
            if (isIncorrect) {
                // Convert the helptext into an array
                let helptext = document.querySelector(
                    `#${this.shelfPrefix}-shelf${shelf}`,
                ).getAttribute('data-helptext').split(';');

                // If no helptext was given, provide a default
                if (helptext.length == 0 ||
                    (helptext.length == 1 && helptext[0] == '')) {
                    helptext = ['Start from the top', 'Nothing comes before something'];
                }

                // Build the tips HTML
                let helptextHTML = `<p>TIPS:</p>
                                    <ul>`;
                for (const index in helptext) {
                    if (Object.hasOwn(helptext, index)) {
                        helptextHTML +=
                            `<li>${helptext[index]}</li>`;
                    }
                }
                helptextHTML += '</ul>';

                if (collection) {
                    helptextHTML = '';
                }
                msgEl.innerHTML = helptextHTML;

                // If any are wrong, set the allCorrect to false
                this.allCorrect = false;
            } else {
                if (!collection) {
                    msgEl.innerHTML = '<p>Great job, you fixed this shelf!</p>';
                }
            }

            // Update the correct and incorrect spans
            const shelfCorrect = document.querySelector(
                `#${this.shelfPrefix}-shelf${shelf} .shelf-correct span`,
            );
            shelfCorrect.innerText = document.querySelectorAll(
                `#${this.shelfPrefix}-shelf${shelf} .correct`,
            ).length;

            const shelfIncorrect = document.querySelector(
                `#${this.shelfPrefix}-shelf${shelf} .shelf-incorrect span`,
            );
            shelfIncorrect.innerText = document.querySelectorAll(
                `#${this.shelfPrefix}-shelf${shelf} .incorrect`,
            ).length;
        }.bind(this));

        return this.allCorrect;
    }

    /**
     * Check the book order, based on the
     * shelvingOrder attribute being a number.
     *
     * @param {Array} books
     * @return {Boolean} isIncorrect
     */
    checkNumericalOrder(books) {
        let isIncorrect = false;

        // Compare the current order with the correct order (by sorting)
        const currentOrder = [];
        books.forEach(function(book) {
            currentOrder.push(Number(book.getAttribute('data-order')));
        });

        // Create a copy of the currentOrder (spread syntax)
        const correctOrder = [...currentOrder];

        // Get the correct order
        correctOrder.sort();

        // Compare book order and apply correct/incorrect class
        currentOrder.forEach(function(bookOrder, index) {
            books[index].classList.remove('incorrect');
            books[index].classList.remove('correct');
            if (bookOrder != correctOrder[index]) {
                books[index].classList.add('incorrect');
                isIncorrect = true;
            } else {
                books[index].classList.add('correct');
            }
        });

        return isIncorrect;
    }

    /**
     * Check the book collection, based on
     * the shelvingOrder attribute being a string.
     * This should match the shelf name where the
     * book belongs.
     *
     * @param {Array} books
     * @param {String} collection
     * @return {Boolean} isIncorrect
     */
    checkCollection(books, collection) {
        let isIncorrect = false;

        // Loop through books and check collection
        books.forEach(function(book, index) {
            // Remove all incorrect/correct classes
            books[index].classList.remove('incorrect');
            books[index].classList.remove('correct');

            // Get the book's collection
            const bookCollection = book.getAttribute('data-order');

            // Check correctness
            if (bookCollection != collection) {
                books[index].classList.add('incorrect');
                isIncorrect = true;
            } else {
                books[index].classList.add('correct');
            }
        });

        return isIncorrect;
    }
}

/**
 * Basic UID for multiple tests
 * https://stackoverflow.com/a/6248722
 *
 * @return {String}
 */
function generateUID() {
    // I generate the UID from two parts here
    // to ensure the random number provide enough bits.
    let firstPart = (Math.random() * 46656) | 0;
    let secondPart = (Math.random() * 46656) | 0;
    firstPart = ('000' + firstPart.toString(36)).slice(-3);
    secondPart = ('000' + secondPart.toString(36)).slice(-3);
    return firstPart + secondPart;
}
