/**
 * This file is based on the tutorial
 * and SCORM API wrapper created by 
 * pipworks.
 * 
 * Tutorial: https://pipwerks.com/2008/05/08/adding-scorm-code-to-an-html-file/
 * SCORM API Wrapper: https://github.com/pipwerks/scorm-api-wrapper
 * SCORM Manifests: https://github.com/pipwerks/SCORM-Manifests
 */

// Initialize Variables
const scorm = pipwerks.SCORM;
let lmsConnected = false;

/**
 * Handle SCORM related errors
 * 
 * @param {string} msg 
 */
function handleScormError(msg) {
  alert(msg);
  window.close();
}

/**
 * Initialize Course
 * 
 * If the LMS connects successfully,
 * set any SCORM status, get learner name,
 * etc.
 * 
 */
function initCourse() {

  //scorm.init returns a boolean
  lmsConnected = scorm.init();

  //If the scorm.init function succeeded...
  if (lmsConnected) {

    //Let's get the completion status to see if the course has already been completed
    var completionstatus = scorm.get("cmi.completion_status");

    //If the course has already been completed...
    if (completionstatus == "completed") {

      //...let's display a message and close the browser window
      handleScormError("You have already completed this course. You do not need to continue.");

    }

    //Now let's get the username from the LMS
    var learnername = scorm.get("cmi.learner_name");

    //If the name was successfully retrieved...
    if (learnername) {

      //...let's display the username in a page element named "learnername"
      document.getElementById("learnername").innerHTML = learnername; //use the name in the form

    }

    //If the course couldn't connect to the LMS for some reason...
  } else {

    //... let's alert the user then close the window.
    handleScormError("Error: Course could not connect with the LMS");

  }

}

/**
 * Set the SCORM Completion Status to Completed,
 * if the LMS connection is successful.
 */
function setComplete() {

  //If the lmsConnection is active...
  if (lmsConnected) {

    //... try setting the course status to "completed"
    var success = scorm.set("cmi.completion_status", "completed");
    var passed = scorm.set("cmi.success_status", "passed");

    //If the course was successfully set to "completed"...
    if (success) {

      //... disconnect from the LMS, we don't need to do anything else.
      scorm.quit();

      //If the course couldn't be set to completed for some reason...
    } else {

      //alert the user and close the course window
      handleScormError("Error: Course could not be set to complete!");

    }

    //If the course isn't connected to the LMS for some reason...
  } else {

    //alert the user and close the course window
    handleScormError("Error: Course is not connected to the LMS");

  }

}